@extends('tenants.layouts.main')

@section('content')

<div class="box">
    <div class="box-header">
        <h3>Detalhes da empresa</h3> 
    </div>

    <div class="box-body">
        <ul>
            <li><strong>Domínio: </strong>
                <td>{{ $company->domain }}</td>
            </li>

            <li><strong>Database: </strong>
                <td>{{ $company->db_database }}</td>
            </li>

            <li><strong>Host: </strong>
                <td>{{ $company->db_hostname }}</td>
            </li>

            <li><strong>Usuário: </strong>
                <td>{{ $company->db_username }}</td>
            </li>

            <li><strong>Senha: </strong>
                <td>secret</td>
            </li>
            
            <form action="{{ route('company.destroy') }}" method="POST">
                <input type="hidden" name="id" value="{{$company->id}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <button class="btn btn-danger btn-sm">Deletar empresa: {{ $company->name }}</button>
            </form>
        </ul>
    </div>
</div>

@endsection