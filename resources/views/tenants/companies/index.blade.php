@extends('tenants.layouts.main')

@section('content')

<div class="box">
    <div class="box-header">
        <h3>Empresas <a href="{{ route('company.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a></h3>
    </div>

    @if (session('success'))
    <div class="alert alert-success">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {{ session('success') }}
    </div>
    @endif

    <div class="box-body">
        @forelse($companies as $company)
            {{ $company->created_at->format('d/m/Y') }} - <span style="color: green">{{ $company->domain }}</span><br>
            {{ $company->name }} <br>
            <a href="{{ route('company.show', [$company->domain]) }}">Detalhes</a> |
            <a href="{{ route('company.edit', [$company->domain]) }}">Editar</a>
            <hr>
        @empty
            <p>Nenhuma empresa cadastrada!</p>
        @endforelse
    </div>
</div>

@endsection