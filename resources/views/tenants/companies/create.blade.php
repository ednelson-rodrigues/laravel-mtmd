@extends('tenants.layouts.main')

@section('content')

<div class="box">
    <div class="box-header">
        <h3>Cadastrar nova empresa</h3>
    </div>

    @if ($errors->any())
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif

    <div class="box-body">
        <form action="{{ route('company.store') }}" method="POST">
        {{ csrf_field() }}
            <div class="form-group">
                <input type="text" class="form-control input-sm" 
                name="name" placeholder="Nome:" value="{{ old('name')}}">
            </div> 

            <div class="form-group">
                <input type="text" class="form-control input-sm" 
                name="domain" placeholder="Domínio:" value="{{ old('domain')}}">
            </div> 

            <div class="form-group">
                <input type="text" class="form-control input-sm" 
                name="db_database" placeholder="Database:" value="{{ old('db_database')}}">
            </div> 

            <div class="form-group">
                <input type="text" class="form-control input-sm" 
                name="db_hostname" placeholder="Host:" value="{{ old('db_hostname')}}">
            </div> 

            <div class="form-group">
                <input type="text" class="form-control input-sm" 
                name="db_username" placeholder="Usuário:" value="{{ old('db_username')}}">
            </div>

            <div class="form-group">
                <input type="password" class="form-control input-sm" 
                name="db_password" placeholder="Senha:" value="{{ old('db_password')}}">
            </div> 

            <div class="form-group">
                <input type="checkbox" id="ck1" name="create_database" checked>
                <label for="ck1">Criar banco de dados?</label>
            </div> 

            <div class="form-group">
                <button type="submit" class="btn btn-success btn-sm"> Enviar</button>
            </div> 
        </form>
    </div>
</div>

@endsection