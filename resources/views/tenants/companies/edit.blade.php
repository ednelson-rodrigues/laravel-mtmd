@extends('tenants.layouts.main')

@section('content')

<div class="box">
    <div class="box-header">
        <h3>Editar empresa: <strong>{{ $company->name }}</strong></h3>
    </div>

    @if ($errors->any())
    <div class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif

    <div class="box-body">
        <form action="{{ route('company.update', $company->id ) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
            <div class="form-group">
                <input type="text" class="form-control input-sm" 
                name="name" placeholder="Nome:" value="{{ $company->name }}">
            </div> 

            <div class="form-group">
                <input type="text" class="form-control input-sm" 
                name="domain" placeholder="Domínio:" value="{{ $company->domain }}">
            </div> 

            <div class="form-group">
                <input type="text" class="form-control input-sm" 
                name="db_database" placeholder="Database:" value="{{ $company->db_database }}">
            </div> 

            <div class="form-group">
                <input type="text" class="form-control input-sm" 
                name="db_hostname" placeholder="Host:" value="{{ $company->db_hostname }}">
            </div> 

            <div class="form-group">
                <input type="text" class="form-control input-sm" 
                name="db_username" placeholder="Usuário:" value="{{ $company->db_username }}">
            </div>

            <div class="form-group">
                <input type="password" class="form-control input-sm" 
                name="db_password" placeholder="Senha:" value="{{ $company->db_password }}">
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success btn-sm"> Atualizar</button>
            </div> 
        </form>
    </div>
</div>

@endsection